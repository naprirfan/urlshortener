#URL Shortener

##Overview

This is a simple URL shortener made by these technology :
1. PHP Laravel
2. Apache
3. MySQL
4. Redis.io
5. AngularJS

##Installation

###Prerequesites: 
1. [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
2. [Virtualbox](https://www.virtualbox.org/)
3. [Vagrant](https://www.vagrantup.com/)

###How to install URL Shortener:
1. Clone this project to your local : `git clone https://naprirfan@bitbucket.org/naprirfan/urlshortener.git`
2. On project's root, execute : `vagrant up`
3. After provisioning complete, go to `http://localhost:8000` to your browser

Enjoy!