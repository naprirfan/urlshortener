<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', "SiteController@index");
Route::get('/{shortcode}', "SiteController@redirectTo");

Route::group(['prefix' => 'api'], function () {
	Route::get('get_recent_item', "ApiController@getRecentItem");
	Route::get('get_most_visited', "ApiController@getMostVisited");
	Route::get('get_total_link_count', "ApiController@getTotalLinkCount");
	Route::post('get_short_url', "ApiController@getShortUrl");
	Route::post('get_custom_short_url', "ApiController@getCustomShortUrl");
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
