<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Dictionary;
use App\Helper;
use Redis;
use Queue;
use App\Jobs\IncrementVisitCountJob;

use Illuminate\Support\Facades\Redirect;

class SiteController extends Controller
{
    public function index()
    {
    	return view('index');
    }

    public function redirectTo($shortcode)
    {
    	//check Redis, is entry exist?
    	//if yes, increment visit count, redirect
        $url = Redis::get($shortcode);
        if ($url && Helper::is_valid_url($url)) {
            $this->incrementVisitCount(Dictionary::where('shortcode', $shortcode)->first());
            return Redirect::away((is_null(parse_url($url, PHP_URL_HOST)) ? '//' : '').$url);
        }

    	//if redis entry not exists, check database
    	//is record exist? create Redis entry, increment visit count, redirect
        //perhaps it's a custom shortcode. E.g : http://bit.ly/someCustomCode
        $data = Dictionary::where('shortcode', $shortcode)->first();
        if (!empty($data)) {
            $this->incrementVisitCount($data);
            Redis::set($shortcode, $data->url);
            return Redirect::away((is_null(parse_url($url, PHP_URL_HOST)) ? '//' : '').$data->url);
        }


    	//if not exist, redirect to 404
        return redirect('/url/notfound');
    }

    private function incrementVisitCount($data)
    {
        Queue::push(new IncrementVisitCountJob($data));
    }
}
