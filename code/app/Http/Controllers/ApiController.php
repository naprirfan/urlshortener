<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Dictionary;
use App\Helper;
use Redis;

class ApiController extends Controller
{
	const CHARACTERS = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

    public function getRecentItem()
    {
    	return response()->json($this->mergeData(Dictionary::take(3)->orderBy('created_at', 'desc')->get()));
    }

    public function getMostVisited()
    {
    	return response()->json($this->mergeData(Dictionary::take(3)->orderBy('visit_count', 'desc')->get()));	
    }

    public function getTotalLinkCount()
    {
    	return response()->json(array('count'=> Dictionary::count()));
    }

    public function getShortUrl(Request $request)
    {
		if (Helper::is_valid_url($request->input('url'))) {
			//check if url already exists : give message!
			$data = Dictionary::where('url', $request->input('url'))->first();
			if (count($data) > 0) {
				return response()->json(array(
                    'id'          => $data->id,
                    'shortcode'   => $data->shortcode,
                    'url'         => $data->url,
                    'visit_count' => $data->visit_count,
                    'created_at'  => $data->created_at,
                    'updated_at'  => $data->updated_at,
                    'message'     => "This URL has already been shortened before"
                ));
			}

            $data = new Dictionary;
            $data->url = $request->input('url');
            $data->visit_count = 0;

			//if requested url not exists
            if (trim($request->input('shortcode')) !== '') {
                //if shortcode already exists
                $check_shortcode = Dictionary::where('shortcode', $request->input('shortcode'))->first();
                if (count($check_shortcode) > 0) {
                    return response()->json(array('error' => 'Ouch! Shortcode already exists for this URL : '. $check_shortcode->url));
                }
                $data->shortcode = $request->input('shortcode');
                $data->save();
            }
            else {
                $data->save();
                $data->shortcode = Helper::dec2any($data->id);
                $data->save();
            }

            //save to redis
            Redis::set($data->shortcode, $request->input('url'));

		 	return response()->json($data);
		} 

		return response()->json(array('error' => 'Ouch!. Please re-check your URL input'));
    }

    /*
    Merge data with full shorturl entry
    */
    private function mergeData($data)
    {   
        $baseurl = array('baseurl' => (string)url('/').'/');
        return array_merge($baseurl, array('items' => $data));
    }
    
}
