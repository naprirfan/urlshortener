<?php
namespace App;

class Helper
{
	const CHARACTERS = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

	public static function dec2any($num, $base=62, $index=false)
	{
		if (! $base ) {
	        $base = strlen( $index );
	    } else if (! $index ) {
	        $index = substr(self::CHARACTERS ,0 ,$base );
	    }
	    $out = "";
	    for ( $t = floor( log10( $num ) / log10( $base ) ); $t >= 0; $t-- ) {
	        $a = floor( $num / pow( $base, $t ) );
	        $out = $out . substr( $index, $a, 1 );
	        $num = $num - ( $a * pow( $base, $t ) );
	    }
	    return $out;
	}
	
	public static function any2dec( $num, $base=62, $index=false ) {
	    if (! $base ) {
	        $base = strlen( $index );
	    } else if (! $index ) {
	        $index = substr(self::CHARACTERS, 0, $base );
	    }
	    $out = 0;
	    $len = strlen( $num ) - 1;
	    for ( $t = 0; $t <= $len; $t++ ) {
	        $out = $out + strpos( $index, substr( $num, $t, 1 ) ) * pow( $base, $len - $t );
	    }
	    return $out;
	}

	public static function is_valid_url($url) {
    	return preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $url);
    }

    public static function is_valid_shortcode($shortcode)	
    {
    	return ctype_alnum($shortcode);
    }
}