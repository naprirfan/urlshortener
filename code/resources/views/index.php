<!DOCTYPE html>
<html lang="en-US" ng-app="urlShortenerApp">
<head>
    <title>Cool URL Shortener</title>

    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css"> <!-- load bootstrap via cdn -->

    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    <script src="js/controllers/mainCtrl.js"></script>
</head>

<body ng-controller="mainCtrl" class="container">

<div class="jumbotron">
    <h1>Shorten your URL :</h1>
    <form ng-submit="processForm()">
        <div class="input-group input-group-lg" style="width:100%">
            <input class="form-control" type="text" ng-model="formdata.url" placeholder="Paste your long URL here">
            <input class="form-control" type="text" ng-model="formdata.shortcode" placeholder="[Optional] Type your custom shortcode if you like; (e.g) foobar, so that url become : {{baseurl}}/foobar">
        </div>
        <br />
        <input class="btn btn-primary btn-lg" type="submit"value="Shorten!" />
    </form>
    <br />
    <div ng-show="message" class="alert alert-warning" role="alert">{{message}}</div>
    
    <div ng-show="error" class="alert alert-danger" role="alert">{{error}}</div>
    
    <div ng-show="shortcode" class="alert alert-success" role="alert">Result : <a target="_blank" href="{{baseurl}}{{shortcode}}">{{baseurl}}{{shortcode}}</a></div>
    
</div>

<h2>Recently Shortened URL</h2>
<img src="img/loading.gif" ng-show="recent_items_is_loading">
<ul class="list-group">
    <li class="list-group-item" ng-repeat="item in recent_items">
        <a target="_blank" href="{{baseurl}}{{item.shortcode}}">{{baseurl}}{{item.shortcode}}</a><br />Goes to :
        <span>{{item.url | excerpt:30}}</span>
        <span class="badge">{{item.visit_count | number}} Hit</span>
    </li>
</ul>

<h2>Most Visited URL</h2>
<img src="img/loading.gif" ng-show="most_visited_is_loading">
<ul class="list-group">
    <li class="list-group-item" ng-repeat="item in most_visited_items">
        <a target="_blank" href="{{baseurl}}{{item.shortcode}}">{{baseurl}}{{item.shortcode}}</a><br />Goes to :
        <span>{{item.url | excerpt:30}}</span>
        <span class="badge">{{item.visit_count | number}} Hit</span>
    </li>
</ul>

<h1>Shortened URL Count : {{totalCount | number}} URLs</h1>

</body>
</html>