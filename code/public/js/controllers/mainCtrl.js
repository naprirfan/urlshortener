var app = angular.module('urlShortenerApp', []);

app.controller('mainCtrl', function($scope, $http) {
	$scope.formdata = {};
	$scope.recent_items_is_loading = true;
	$scope.most_visited_is_loading = true;

	$http.get('/api/get_recent_item').success(function(data) {
	    $scope.recent_items = data.items;
	    $scope.baseurl = data.baseurl;
	    $scope.recent_items_is_loading = false;
  	});
	$http.get('/api/get_most_visited').success(function(data) {
	    $scope.most_visited_items = data.items;
	    $scope.baseurl = data.baseurl;
	    $scope.most_visited_is_loading = false;
  	});
  	$http.get('/api/get_total_link_count').success(function(data) {
    	$scope.totalCount = data.count;
  	});
	$scope.processForm = function() {
		$http.post('/api/get_short_url', $scope.formdata)
		.success(function(data) {
			$scope.shortcode = data.shortcode;
			$scope.error = data.error;
			$scope.message = data.message;
			$http.get('/api/get_total_link_count').success(function(data) {
		    	$scope.totalCount = data.count;
		  	});	
		  	$http.get('/api/get_most_visited').success(function(data) {
			    $scope.most_visited_items = data.items;
		  	});
		  	$http.get('/api/get_recent_item').success(function(data) {
			    $scope.recent_items = data.items;
		  	});
		});
	};
});

app.filter('excerpt', function(){
	return function(text, length) {
		if (text.length > length) {
			return text.substr(0, length) + '...';
		}
		return text;
	}
});